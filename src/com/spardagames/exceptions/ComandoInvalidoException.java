package com.spardagames.exceptions;

public class ComandoInvalidoException extends RuntimeException {

	public ComandoInvalidoException(String message) {
		super(message);
	}

}
