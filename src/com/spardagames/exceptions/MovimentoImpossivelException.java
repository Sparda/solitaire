package com.spardagames.exceptions;

public class MovimentoImpossivelException extends RuntimeException{

	public MovimentoImpossivelException(String message) {
		super(message);
	}

}
